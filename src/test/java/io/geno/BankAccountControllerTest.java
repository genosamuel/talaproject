package io.geno;

/**
 * Created by geno on 03/12/2017.
 */
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;

import io.geno.controller.BankAccountController;
import io.geno.repository.BankAccountRepository;
import io.geno.service.BankAccountService;
import io.geno.utils.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BankAccountController.class, secure = false)
public class BankAccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BankAccountService bankAccountService;

    @MockBean
    private BankAccountRepository bankAccountRepository;


    @Test
    public void firstTimeDepositMaxPerTransactionExceeded() throws Exception {

        bankAccountRepository.deleteAll();

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 50000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void firstTimeDepositOk() throws Exception {

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 40000));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 40000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void maxPerTransactionExceededOnSubsequentDeposits() throws Exception {

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 50000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void depositOkOnSubsequentAttempts() throws Exception {

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 80000));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 40000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 120000));

        result = mockMvc.perform(requestBuilder).andReturn();

        response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }
    /**
     * Covers how controller handles maximum deposit amount in a day
     */
    @Test
    public void maxDepositPerDayExceeded() throws Exception {

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 40000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    /**
     * Allows us to reach the max number of deposit transactions in a day such that subsequent attempts fail
     */
    @Test
    public void lastDepositOfDayOk() throws Exception {

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 150000));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 30000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void fifthDepositOfDayNotOk() throws Exception {

        Mockito.when(
                bankAccountService.deposit( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/deposit")
                .accept(MediaType.APPLICATION_JSON).content("{\"depositAmount\": 30000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void balanceLessThanWithdrawalAmount() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 250000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void firstTimeWithdrawalMaxPerTransactionExceeded() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 50000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void firstTimeWithdrawalOk() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 130000));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 20000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void maxPerTransactionExceededOnSubsequentWithdrawals() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 50000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void withdrawOkOnSubsequentAttempts() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 110000));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 20000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    /**
     * Covers how controller handles maximum withdrawal amount in a day
     */
    @Test
    public void maxWithdrawalPerDayExceeded() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 20000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }
    /**
     * Allows us to reach the max number of withdrawal transactions in a day such that subsequent attempts fail
     */
    @Test
    public void lastWithdrawalOfDayOk() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(true, 100000));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 10000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void fourthWithdrawalOfDayNotOk() throws Exception {

        Mockito.when(
                bankAccountService.withdraw( Mockito.any(BigDecimal.class))).thenReturn(new Pair(false, ""));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/bankaccount/withdraw")
                .accept(MediaType.APPLICATION_JSON).content("{\"withdrawAmount\": 10000}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

    }

    @Test
    public void balance() throws Exception {

        Mockito.when(
                bankAccountService.getBalance()).thenReturn("Your current balance is 100,000");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/bankaccount/balance").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }



}


