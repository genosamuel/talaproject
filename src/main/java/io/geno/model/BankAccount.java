package io.geno.model;

/**
 * Created by geno on 03/12/2017.
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The entity class used to persist bank account data
 */
@Entity
public class BankAccount {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private BigDecimal balance;
    private BigDecimal amountDepositedForTheDay;
    private BigDecimal amountWithdrawnForTheDay;
    private int numberOfDepositsForTheDay;
    private int numberOfWithdrawalsForTheDay;
    private LocalDate lastDepositDate;
    private LocalDate lastWithdrawalDate;

    protected BankAccount() {}


    public BankAccount(BigDecimal balance, BigDecimal amountDepositedForTheDay, BigDecimal amountWithdrawnForTheDay, int numberOfWithdrawalsForTheDay,
                       int numberOfDepositsForTheDay) {
        this.balance = balance;
        this.amountDepositedForTheDay = amountDepositedForTheDay;
        this.amountWithdrawnForTheDay = amountWithdrawnForTheDay;
        this.numberOfDepositsForTheDay = numberOfDepositsForTheDay;
        this.numberOfWithdrawalsForTheDay = numberOfWithdrawalsForTheDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getNumberOfDepositsForTheDay() {
        return numberOfDepositsForTheDay;
    }

    public void setNumberOfDepositsForTheDay(int numberOfDepositsForTheDay) {
        this.numberOfDepositsForTheDay = numberOfDepositsForTheDay;
    }

    public int getNumberOfWithdrawalsForTheDay() {
        return numberOfWithdrawalsForTheDay;
    }

    public void setNumberOfWithdrawalsForTheDay(int numberOfWithdrawalsForTheDay) {
        this.numberOfWithdrawalsForTheDay = numberOfWithdrawalsForTheDay;
    }

    public LocalDate getLastDepositDate() {
        return lastDepositDate;
    }

    public void setLastDepositDate(LocalDate lastDepositDate) {
        this.lastDepositDate = lastDepositDate;
    }

    public LocalDate getLastWithdrawalDate() {
        return lastWithdrawalDate;
    }

    public void setLastWithdrawalDate(LocalDate lastWithdrawalDate) {
        this.lastWithdrawalDate = lastWithdrawalDate;
    }

    public BigDecimal getAmountDepositedForTheDay() {
        return amountDepositedForTheDay;
    }

    public void setAmountDepositedForTheDay(BigDecimal amountDepositedForTheDay) {
        this.amountDepositedForTheDay = amountDepositedForTheDay;
    }

    public BigDecimal getAmountWithdrawnForTheDay() {
        return amountWithdrawnForTheDay;
    }

    public void setAmountWithdrawnForTheDay(BigDecimal amountWithdrawnForTheDay) {
        this.amountWithdrawnForTheDay = amountWithdrawnForTheDay;
    }

    @Override
    public String toString() {
        return String.format(
                "BankAccount [id=%s, balance=%s, numberOfWithdrawalsForTheDay=%s, numberOfDepositsForTheDay=%s]", id,
                balance, numberOfWithdrawalsForTheDay, numberOfDepositsForTheDay);
    }
}
