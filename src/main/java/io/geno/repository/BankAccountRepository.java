package io.geno.repository;

/**
 * Created by geno on 03/12/2017.
 *
 * Data repository that allows us to read and write bank account data
 */

import io.geno.model.BankAccount;
import org.springframework.data.repository.CrudRepository;

public interface BankAccountRepository extends CrudRepository<BankAccount, Long> {

}
