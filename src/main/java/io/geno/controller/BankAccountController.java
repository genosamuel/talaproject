package io.geno.controller;

/**
 * Created by geno on 03/12/2017.
 *
 * Exposes the 3 required web services for getting balance, depositing and withdrawing money
 */
import io.geno.service.BankAccountService;
import io.geno.utils.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;

@RestController
public class BankAccountController {

    @Autowired
    private BankAccountService bankAccountService;

    @GetMapping("/bankaccount/balance")
    public String getBalance() {
        return bankAccountService.getBalance();
    }

    @PostMapping("/bankaccount/deposit")
    public ResponseEntity deposit(@RequestBody HashMap<String, BigDecimal> parameters) {

        Pair<Boolean, Object> result = bankAccountService.deposit(parameters.get("depositAmount"));

        if(result.getFirst()) {
            return ResponseEntity.ok(result.getSecond());
        }else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result.getSecond());
        }

    }

    @PostMapping("/bankaccount/withdraw")
    public ResponseEntity withdraw(@RequestBody HashMap<String, BigDecimal> parameters) {

        Pair<Boolean, Object> result = bankAccountService.withdraw(parameters.get("withdrawAmount"));

        if(result.getFirst()) {
            return ResponseEntity.ok(result.getSecond());
        }else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result.getSecond());
        }

    }

}
