package io.geno.utils;

import java.text.DecimalFormat;

/**
 * Created by geno on 03/12/2017.
 */
public class Utils {
    public static String formatDecimal(String currencySymbol, Object d){
        if(currencySymbol != null)
            return new DecimalFormat(currencySymbol+" ###,###,##0.##").format(d);
        else
            return new DecimalFormat("###,###,##0.##").format(d);
    }
}
