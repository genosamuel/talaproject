package io.geno;

import io.geno.model.BankAccount;
import io.geno.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Created by geno on 03/12/2017.
 *
 * DataLoader creates a bank account object and saves it in case we do not have any
 */
@Component
public class DataLoader implements ApplicationRunner {

    private BankAccountRepository bankAccountRepository;

    @Autowired
    public DataLoader(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    public void run(ApplicationArguments args) {
        if(bankAccountRepository.count() == 0) {
            bankAccountRepository.save(new BankAccount(BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), 0, 0));
        }
    }
}
