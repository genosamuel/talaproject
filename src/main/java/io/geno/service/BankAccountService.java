package io.geno.service;

/**
 * Created by geno on 03/12/2017.
 *
 * The service class used by bank account controller to access bank account repository
 */
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;

import io.geno.model.BankAccount;
import io.geno.repository.BankAccountRepository;
import io.geno.utils.Pair;
import io.geno.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class BankAccountService {

    public final static  String ZONE_NAIROBI = "Africa/Nairobi";
    public final static BigDecimal MAX_DEPOSIT_FOR_THE_DAY = BigDecimal.valueOf(150000);
    public final static BigDecimal MAX_DEPOSIT_PER_TRANSACTION = BigDecimal.valueOf(40000);
    public final static int MAX_DEPOSIT_TRANSACTIONS_PER_DAY = 4;

    public final static BigDecimal MAX_WITHDRAWAL_FOR_THE_DAY = BigDecimal.valueOf(50000);
    public final static BigDecimal MAX_WITHDRAWAL_PER_TRANSACTION = BigDecimal.valueOf(20000);
    public final static int MAX_WITHDRAWAL_TRANSACTIONS_PER_DAY = 3;


    @Autowired
    BankAccountRepository repository;

    public String getBalance() {
        return String.format(
                "Your current balance is %s", Utils.formatDecimal(null, repository.findAll().iterator().next().getBalance())) ;
    }

    @Transactional
    public Pair<Boolean, Object> deposit( BigDecimal depositAmount) {

        Pair<Boolean, Object> result = new Pair(false, "");

        BankAccount bankAccount = repository.findAll().iterator().next();
        LocalDate today = LocalDate.now(ZoneId.of(ZONE_NAIROBI));

            if(bankAccount.getLastDepositDate() != null && bankAccount.getLastDepositDate().equals(today)){
                if(bankAccount.getNumberOfDepositsForTheDay() < MAX_DEPOSIT_TRANSACTIONS_PER_DAY ){
                    BigDecimal amountToBeDepositedForTheDay = bankAccount.getAmountDepositedForTheDay().add(depositAmount);
                    if(amountToBeDepositedForTheDay.compareTo(MAX_DEPOSIT_FOR_THE_DAY) <= 0){
                        if(depositAmount.compareTo(MAX_DEPOSIT_PER_TRANSACTION) <= 0) {
                            bankAccount.setBalance(bankAccount.getBalance().add(depositAmount));
                            bankAccount.setNumberOfDepositsForTheDay(bankAccount.getNumberOfDepositsForTheDay() + 1);
                            bankAccount.setAmountDepositedForTheDay(bankAccount.getAmountDepositedForTheDay().add(depositAmount));
                            repository.save(bankAccount);
                            result.setFirst(true);
                            result.setSecond(bankAccount.getBalance());
                        }else{
                            result.setSecond("Max deposit amount per transaction of 40,000 exceeded");
                        }
                    }else{
                        result.setSecond("Max deposit amount per day of 150,000 exceeded");
                    }
                }else{
                    result.setSecond("Max number of deposit transactions per day exceeded");
                }

            }else{
                if(depositAmount.compareTo(MAX_DEPOSIT_PER_TRANSACTION) <= 0){
                    bankAccount.setBalance(bankAccount.getBalance().add(depositAmount));
                    bankAccount.setLastDepositDate(today);
                    bankAccount.setNumberOfDepositsForTheDay(1);
                    bankAccount.setAmountDepositedForTheDay(depositAmount);
                    repository.save(bankAccount);
                    result.setFirst(true);
                    result.setSecond(bankAccount.getBalance());
                }else{
                    result.setSecond("Max deposit per transaction of 40,000 exceeded");
                }
            }


        return result;
    }

    @Transactional
    public Pair<Boolean, Object> withdraw( BigDecimal withdrawAmount) {

        Pair<Boolean, Object> result = new Pair(false, "");

        BankAccount bankAccount = repository.findAll().iterator().next();
        LocalDate today = LocalDate.now(ZoneId.of(ZONE_NAIROBI));

        if(bankAccount.getBalance().compareTo(withdrawAmount) >= 0) {
            if (bankAccount.getLastWithdrawalDate() != null && bankAccount.getLastWithdrawalDate().equals(today)) {
                if (bankAccount.getNumberOfWithdrawalsForTheDay() < MAX_WITHDRAWAL_TRANSACTIONS_PER_DAY) {
                    BigDecimal amountToBeWithdrawnForTheDay = bankAccount.getAmountWithdrawnForTheDay().add(withdrawAmount);
                    if (amountToBeWithdrawnForTheDay.compareTo(MAX_WITHDRAWAL_FOR_THE_DAY) <= 0) {
                        if (withdrawAmount.compareTo(MAX_WITHDRAWAL_PER_TRANSACTION) <= 0) {
                            bankAccount.setBalance(bankAccount.getBalance().subtract(withdrawAmount));
                            bankAccount.setNumberOfWithdrawalsForTheDay(bankAccount.getNumberOfWithdrawalsForTheDay() + 1);
                            bankAccount.setAmountWithdrawnForTheDay(bankAccount.getAmountWithdrawnForTheDay().add(withdrawAmount));
                            repository.save(bankAccount);
                            result.setFirst(true);
                            result.setSecond(bankAccount.getBalance());
                        } else {
                            result.setSecond("Max withdrawal amount per transaction of 20,000 exceeded");
                        }
                    } else {
                        result.setSecond("Max withdrawal amount per day of 50,000 exceeded");
                    }
                } else {
                    result.setSecond("Max number of withdrawal transactions per day exceeded");
                }

            } else {
                if (withdrawAmount.compareTo(MAX_WITHDRAWAL_PER_TRANSACTION) <= 0) {
                    bankAccount.setBalance(bankAccount.getBalance().subtract(withdrawAmount));
                    bankAccount.setLastWithdrawalDate(today);
                    bankAccount.setNumberOfWithdrawalsForTheDay(1);
                    bankAccount.setAmountWithdrawnForTheDay(withdrawAmount);
                    repository.save(bankAccount);
                    result.setFirst(true);
                    result.setSecond(bankAccount.getBalance());
                } else {
                    result.setSecond("Max withdrawal amount per transaction of 20,000 exceeded");
                }
            }
        }else{
            result.setSecond("Balance is less than withdrawal amount");
        }


        return result;
    }

}