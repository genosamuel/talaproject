# README #

## Tala Take-home Test ###

### How do I run it? ###

* You can use docker by typing the following command in your terminal

 > `docker run -p 8080:8080 deftgeno/talaproject:latest`

### How do I test it? ###

* Use [postman](https://www.getpostman.com/) to test the deposit and withdraw services

  > For deposit use `http://localhost:8080/bankaccount/deposit`


  > Add header `Content-Type: application/json`



  > Under body select raw and use json similar to ``{"depositAmount": 40000}``

  > For withdraw service use `http://localhost:8080/bankaccount/withdraw`


  > Add header `Content-Type: application/json`


  > Under body select raw and use json similar to ``{"withdrawAmount": 20000}``

* Enter http://localhost:8080/bankaccount/balance in your browser to test the balance service

### How to view code coverage report ###

* clone by entering the following command in your terminal

 > `git clone https://genosamuel@bitbucket.org/genosamuel/talaproject.git`

* Go to the code_coverage folder and open index.html in your browser